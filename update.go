package main

import (
	"fmt"
	"log"

	"gitlab.com/norad/relay-cli/relay"
	"gopkg.in/urfave/cli.v1"
)

func updateHandler(c *cli.Context) error {
	docker_client, err := relay.NewRelayDockerClient()
	if err != nil {
		panic(err)
	}
	enabler := relay.RelayEnabler{
		Config: relay.EnableConfig{
			ConfigDir: c.String("dir"),
			Force:     false,
		},
		Docker: relay.RelayDocker{
			Client: docker_client,
		},
	}

	disabler := relay.RelayDisabler{
		Docker: relay.RelayDocker{
			Client: docker_client,
		},
	}

	container_config, err := enabler.LoadContainerConfig()
	if err != nil {
		log.Printf("Unable to load the Relay configuration.")
		log.Printf("Relay update failed.")
		if err.Error() == "Outdated Relay file." {
			log.Printf("Run `relay fixup-config --help` for more information")
		}
		return err
	}

	installer := relay.RelayInstaller{
		Docker: relay.RelayDocker{
			Client: docker_client,
		},
		Config: relay.InstallConfig{
			InstallDir: c.String("dir"),
		},
		Relay: relay.Relay{
			Image: container_config.Image,
		},
	}

	updater := relay.RelayUpdater{
		Image: container_config.Image,
		Docker: relay.RelayDocker{
			Client: docker_client,
		},
	}

	if c.Bool("force") || confirmUpdate() {
		return relay.Update(updater, installer, enabler, disabler)
	} else {
		log.Printf("Exiting without updating.")
		return nil
	}
}

func confirmUpdate() bool {
	fmt.Println("Are you sure you want to update the Relay? Please enter 'yes' or 'no'.")
	return confirmAction()
}
