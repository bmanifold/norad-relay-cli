package relay

import (
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"io/ioutil"
	"log"
)

type RelayKey interface {
	InitializeKey() error
	Fingerprint() (string, error)
}

type RelaySigningKey struct {
	Privkey *rsa.PrivateKey
	File    string
}

func (key *RelaySigningKey) InitializeKey() error {
	if key.File == "" {
		log.Printf("No path to key has been specified.")
		return errors.New("No path to key has been specified.")
	}
	raw_bytes, err := ioutil.ReadFile(key.File)

	if err != nil {
		log.Printf("Unable to read keyfile %s.", key.File)
		log.Print(err)
		return err
	}

	block, _ := pem.Decode(raw_bytes)
	if block == nil {
		log.Printf("Unable to load key from %s.", key.File)
		return errors.New("Unable to load key")
	}

	privkey, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		log.Printf("Key format is invalid for key %s.", key.File)
		log.Print(err)
		return err
	}
	key.Privkey = privkey
	return nil
}

func (key *RelaySigningKey) Fingerprint() ([]byte, error) {
	if key.Privkey == nil {
		if err := key.InitializeKey(); err != nil {
			return []byte{}, err
		}
	}

	asn1bytes, err := x509.MarshalPKIXPublicKey(&key.Privkey.PublicKey)
	if err != nil {
		log.Printf("Unable to load public key from provided private key.")
		log.Print(err)
		return []byte{}, err
	}

	sum := sha256.Sum256(asn1bytes)
	return sum[:], nil
}
