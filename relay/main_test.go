package relay

import (
	"io/ioutil"
	"log"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	log.SetFlags(0)
	log.SetOutput(ioutil.Discard)
	os.Exit(m.Run())
}
