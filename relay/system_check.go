package relay

import (
	"context"
	"errors"
	"log"
	"net"
	"strings"
	"time"
)

type SystemChecker interface {
	CheckDocker() error
	CheckApiConnectivity() error
	CheckRegistryConnectivity() error
	ProcessNetworkError(error)
}

type RelaySystemChecker struct {
	ApiUri       string
	RegistryUris []string
	ConnTimeout  time.Duration
	Docker       RelayDocker
}

func (checker RelaySystemChecker) CheckDocker() error {
	_, err := checker.Docker.Client.ServerVersion(context.Background())
	if err != nil {
		log.Printf("Unable to detect Docker installation.")
		log.Printf("The following error occurred while trying to connect to Docker: %s", err)
		return err
	}

	log.Printf("Successfully connected to the Docker daemon.")
	return nil
}

func (checker RelaySystemChecker) ProcessNetworkError(err error) {
	if net_err, ok := err.(*net.OpError); ok {
		if net_err.Addr == nil {
			log.Printf("It appears that the CLI tool is unable to resolve the requested host. Are your DNS settings correct?")
		} else if strings.Contains(net_err.Err.Error(), "timeout") {
			log.Printf("It appears that the CLI tool is unable to connect to the requested host. Are your firewall settings properly configured?")
		}
	}
	log.Printf("The raw error message is: %s", err)
}

func (checker RelaySystemChecker) CheckApiConnectivity() error {
	log.Printf("Attempting to connect to the API host at %s.", checker.ApiUri)
	dialer := net.Dialer{Timeout: checker.ConnTimeout}
	_, err := dialer.Dial("tcp", checker.ApiUri)
	if err != nil {
		log.Printf("Unable to connect to the API host at %s.", checker.ApiUri)
		checker.ProcessNetworkError(err)
	} else {
		log.Printf("Successfully connected to the API host at %s.", checker.ApiUri)
	}
	return err
}

func (checker RelaySystemChecker) CheckRegistryConnectivity() error {
	any_errors := false
	dialer := net.Dialer{Timeout: checker.ConnTimeout}
	for i := range checker.RegistryUris {
		log.Printf("Attempting to connect to the Registry host at %s.", checker.RegistryUris[i])
		_, err := dialer.Dial("tcp", checker.RegistryUris[i])
		if err != nil {
			log.Printf("Unable to connect to the Registry host at %s.", checker.RegistryUris[i])
			checker.ProcessNetworkError(err)
			any_errors = true
		} else {
			log.Printf("Successfully connected to the Registry host at %s.", checker.RegistryUris[i])
		}
	}
	if any_errors {
		return errors.New("One ore more errors ocurred while trying to connect to Registry hosts.")
	} else {
		return nil
	}
}

func SystemCheck(checker SystemChecker) error {
	check_errors := make([]error, 3)
	check_errors[0] = checker.CheckDocker()
	check_errors[1] = checker.CheckApiConnectivity()
	check_errors[2] = checker.CheckRegistryConnectivity()

	for i := range check_errors {
		if check_errors[i] != nil {
			return errors.New("One or more system checks failed.")
		}
	}
	return nil
}
