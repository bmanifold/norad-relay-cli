package relay

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/stretchr/testify/mock"
)

type MockDisabler struct {
	mock.Mock
}

func (d *MockDisabler) FindContainer() (string, error) {
	args := d.Called()
	return args.String(0), args.Error(1)
}

func (d *MockDisabler) StopContainer(cid string) error {
	args := d.Called()
	return args.Error(0)
}

func (d *MockDisabler) RemoveContainer(cid string) error {
	args := d.Called()
	return args.Error(0)
}

func TestDisable(t *testing.T) {
	cases := []struct {
		find_container   error
		stop_container   error
		remove_container error
		container_id     string
		expectation      error
	}{
		{errors.New("1"), nil, nil, "foo", errors.New("1")},
		{nil, errors.New("2"), nil, "foo", errors.New("2")},
		{nil, nil, errors.New("3"), "foo", errors.New("3")},
		{nil, nil, nil, "", nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing execution halting for method %s", c.expectation), func(t *testing.T) {
			testObj := new(MockDisabler)
			testObj.On("FindContainer").Return(c.container_id, c.find_container)
			testObj.On("StopContainer").Return(c.stop_container)
			testObj.On("RemoveContainer").Return(c.remove_container)
			ret := Disable(testObj)

			if c.expectation != nil && ret.Error() != c.expectation.Error() {
				t.Errorf("Expected Error %s, got %s", c.expectation, ret)
			} else if c.expectation == nil && ret != nil {
				t.Errorf("Expected no error, got %s", ret)
			}
		})
	}
}

func TestRelayDisablerFindContainer(t *testing.T) {
	cases := []struct {
		hasRelay      bool
		numContainers int
		expectedError error
	}{
		{numContainers: 2},
		{numContainers: 2, hasRelay: true, expectedError: errors.New("list_error")},
		{numContainers: 2, hasRelay: true},
		{false, 0, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Searching for Relay container"), func(t *testing.T) {
			containers := containersGeneratorForFind(c.hasRelay, c.numContainers)
			client := MockDockerClient{}
			client.On(
				"ContainerList",
				context.Background(),
				types.ContainerListOptions{All: true},
			).Return(containers, c.expectedError)
			disabler := RelayDisabler{
				Docker: RelayDocker{Client: &client},
			}
			cid, err := disabler.FindContainer()
			if c.expectedError == nil {
				if err != nil {
					t.Errorf("Expected no error, got %s", err)
				} else if c.hasRelay && cid != "therelay" {
					t.Errorf("Expected to find the Relay container, but did not")
				} else if !c.hasRelay && cid != "" {
					t.Errorf("Not expecting to find the relay, but did")
				}
			} else {
				if err == nil {
					t.Errorf("Expected to encounter an error, but none returned")
				}
			}
		})
	}
}

func TestRelayDisablerStopContainer(t *testing.T) {
	cases := []struct {
		cid            string
		expected_error error
	}{
		{"foo", nil},
		{"bar", nil},
		{"baz", errors.New("1")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing stopping container for %s", c.cid), func(t *testing.T) {
			timeout := time.Duration(60) * time.Second
			client := MockDockerClient{}
			client.On(
				"ContainerStop",
				context.Background(),
				mock.MatchedBy(
					func(input string) bool {
						return input == c.cid
					},
				),
				&timeout,
			).Return(c.expected_error)
			disabler := RelayDisabler{
				Docker: RelayDocker{Client: &client},
			}
			ret := disabler.StopContainer(c.cid)
			client.AssertNumberOfCalls(t, "ContainerStop", 1)
			if c.expected_error == nil && ret != nil {
				t.Errorf("Expected no error, but received %s", ret)
			} else if c.expected_error != nil && ret == nil {
				t.Errorf("Expected an error, but none received")
			}
		})
	}
}

func TestRelayDisablerRemoveContainer(t *testing.T) {
	cases := []struct {
		cid            string
		expected_error error
	}{
		{"foo", nil},
		{"bar", nil},
		{"baz", errors.New("1")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing removing container for %s", c.cid), func(t *testing.T) {
			client := MockDockerClient{}
			client.On(
				"ContainerRemove",
				context.Background(),
				mock.MatchedBy(
					func(input string) bool {
						return input == c.cid
					},
				),
				types.ContainerRemoveOptions{RemoveVolumes: true, Force: true},
			).Return(c.expected_error)
			disabler := RelayDisabler{
				Docker: RelayDocker{Client: &client},
			}
			ret := disabler.RemoveContainer(c.cid)
			client.AssertNumberOfCalls(t, "ContainerRemove", 1)
			if c.expected_error == nil && ret != nil {
				t.Errorf("Expected no error, but received %s", ret)
			} else if c.expected_error != nil && ret == nil {
				t.Errorf("Expected an error, but none received")
			}
		})
	}
}
