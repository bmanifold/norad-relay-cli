package relay

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"github.com/docker/docker/api/types"
	"github.com/stretchr/testify/mock"
)

type MockDocker struct {
	mock.Mock
}

func (helper MockDocker) RetrieveTests(all bool, exited bool) ([]types.Container, error) {
	args := helper.Called()
	return args.Get(0).([]types.Container), args.Error(1)
}

func (helper MockDocker) RemoveContainers([]types.Container) error {
	args := helper.Called()
	return args.Error(0)
}

func containersGeneratorForFind(hasRelay bool, numContainers int) []types.Container {
	containers := make([]types.Container, numContainers)
	for i := 0; i < numContainers; i++ {
		container := types.Container{ID: "foo", Names: make([]string, 1)}
		containers = append(containers, container)
	}
	if numContainers > 0 && hasRelay {
		containers[0].ID = "therelay"
		containers[0].Names = append(containers[0].Names, "/norad_relay")
	}
	return containers
}

// This is mostly a passthrough method that wraps the Docker library's ContainerList function. With
// that in mind, we'll focus on testing that the proper filtering values are set.
func TestRelayDockerRetrieveTests(t *testing.T) {
	cases := []struct {
		exited         bool
		all            bool
		expected_error error
	}{
		{true, true, nil},
		{true, false, nil},
		{false, true, nil},
		{false, false, nil},
		{true, true, errors.New("1")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing behavior for exited %t, all %t, and error %s", c.exited, c.all, c.expected_error), func(t *testing.T) {
			client := MockDockerClient{}
			client.On(
				"ContainerList",
				context.Background(),
				mock.MatchedBy(
					func(input types.ContainerListOptions) bool {
						name_filter := input.Filters.Get("name")
						status_filter := input.Filters.Get("status")
						status_ret_value := false
						if c.exited && len(status_filter) > 0 && status_filter[0] == "exited" {
							status_ret_value = true
						} else if !c.exited && len(status_filter) == 0 {
							status_ret_value = true
						}
						return input.All == c.all && name_filter[0] == "noradjob*" && status_ret_value
					},
				),
			).Return([]types.Container{}, nil)
			relay_docker := RelayDocker{Client: &client}
			relay_docker.RetrieveTests(c.all, c.exited)
		})
	}
}

func TestRelayDockerRemoveContainers(t *testing.T) {
	cases := []struct {
		num_calls      int
		expected_error error
	}{
		{0, nil},
		{1, nil},
		{3, nil},
		{0, errors.New("0")},
		{1, errors.New("1")},
		{3, errors.New("3")},
	}
	for _, c := range cases {
		t.Run(fmt.Sprintf("Sending %d calls and returning %s error", c.num_calls, c.expected_error), func(t *testing.T) {
			containers := make([]types.Container, c.num_calls)
			for i := range containers {
				containers[i].ID = "foo"
				containers[i].Names = []string{"1", "2"}
			}
			client := MockDockerClient{}
			client.On(
				"ContainerRemove",
				context.Background(),
				"foo",
				types.ContainerRemoveOptions{RemoveVolumes: true, Force: true},
			).Return(c.expected_error)
			relay_docker := RelayDocker{Client: &client}
			relay_docker.RemoveContainers(containers)
			if c.num_calls == 0 {
				client.AssertNumberOfCalls(t, "ContainerRemove", 0)
			} else if c.expected_error == nil {
				client.AssertNumberOfCalls(t, "ContainerRemove", c.num_calls)
			} else {
				client.AssertNumberOfCalls(t, "ContainerRemove", 1)
			}
		})
	}
}
