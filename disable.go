package main

import (
	"fmt"
	"log"

	"gitlab.com/norad/relay-cli/relay"
	"gopkg.in/urfave/cli.v1"
)

func disableHandler(c *cli.Context) error {
	docker_client, err := relay.NewRelayDockerClient()
	if err != nil {
		panic(err)
	}

	disabler := relay.RelayDisabler{
		Docker: relay.RelayDocker{
			Client: docker_client,
		},
	}
	if c.Bool("force") || confirmDisable() {
		return relay.Disable(disabler)
	} else {
		log.Printf("Exiting without disabling.")
		return nil
	}
}

func confirmDisable() bool {
	fmt.Println("Are you sure you want to disable the Relay? Please enter 'yes' or 'no'.")
	return confirmAction()
}
