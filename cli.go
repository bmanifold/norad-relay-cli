package main

import (
	"log"
	"os"
	"os/user"
	"sort"

	"gopkg.in/urfave/cli.v1"
)

var dry_run bool

func init() {
	if cuser, _ := user.Current(); cuser.Uid != "0" {
		log.Fatal("This utility must be executed as root!")
	}
}

func main() {
	app := configureApp()
	app.Action = func(c *cli.Context) error {
		cli.ShowAppHelp(c)
		return nil
	}

	sort.Sort(cli.FlagsByName(app.Flags))
	sort.Sort(cli.CommandsByName(app.Commands))
	app.Run(os.Args)
}

func configureApp() *cli.App {
	app := cli.NewApp()
	app.Name = "relay"
	app.Usage = "manage your Norad Relay installation"
	app.Version = Version

	app.Commands = []cli.Command{
		{
			Name:        "install",
			Usage:       "installs the Norad Relay to the host",
			Description: "Downloads and installs the Norad Relay to this host. If the Relay is already installed, this command has no effect",
			Action:      installHandler,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "dir",
					Value: "/etc/norad.d",
					Usage: "installation `DIRECTORY` for Relay configuration",
				},
				cli.StringFlag{
					Name:  "relay-image",
					Value: "norad-registry.cisco.com:5000/relay:latest",
					Usage: "The `IMAGE` name of the Norad Relay Docker image",
				},
				cli.StringFlag{
					Name:   "organization-token",
					Usage:  "[REQUIRED] The `TOKEN` of the Organization for the Relay",
					EnvVar: "NORAD_ORGANIZATION_TOKEN",
				},
				cli.StringFlag{
					Name:  "api-uri",
					Value: "https://norad.cisco.com:8443/v1",
					Usage: "The `URI` and path of the associated Norad instance",
				},
				cli.StringFlag{
					Name:  "queue-user",
					Value: "norad-consumer",
					Usage: "The RabbitMQ `USER` for authenticating with the scan queue",
				},
				cli.StringFlag{
					Name:  "queue-password",
					Value: "norad-consumer",
					Usage: "The RabbitMQ `PASSWORD` for authenticating with the scan queue",
				},
				cli.BoolFlag{
					Name:  "no-enable",
					Usage: "Do not enable the Relay after installing",
				},
				cli.BoolFlag{
					Name:        "dry-run, n",
					Usage:       "print the actions the command would take without actually doing them",
					Destination: &dry_run,
				},
			},
		},
		{
			Name:        "uninstall",
			Usage:       "removes the Norad Relay from the host",
			Description: "Removes the Relay and deletes its configuration directory",
			Action:      uninstallHandler,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "dir",
					Value: "/etc/norad.d",
					Usage: "`DIRECTORY` with Norad configuration",
				},
				cli.StringFlag{
					Name:  "relay-image",
					Value: "norad-registry.cisco.com:5000/relay:latest",
					Usage: "The `IMAGE` name of the Norad Relay Docker image",
				},
				cli.BoolFlag{
					Name:  "force, f",
					Usage: "don't ask for confirmation before removing",
				},
			},
		},
		{
			Name:        "enable",
			Usage:       "enables the Norad Relay",
			Description: "Creates and starts the Norad Relay container",
			Action:      enableHandler,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "dir",
					Value: "/etc/norad.d",
					Usage: "`DIRECTORY` with Norad configuration",
				},
				cli.BoolFlag{
					Name:  "force, f",
					Usage: "Enable the Norad Relay even if an existing Relay is found.",
				},
			},
		},
		{
			Name:        "disable",
			Usage:       "disables the Norad Relay",
			Description: "Stops and removes the Norad Relay container while preserving configuration data.",
			Action:      disableHandler,
			Flags: []cli.Flag{
				cli.BoolFlag{
					Name:  "force, f",
					Usage: "Do not prompt for confirmation before disabling the Relay.",
				},
			},
		},
		{
			Name:        "restart",
			Usage:       "restarts the Norad Relay",
			Description: "Stops and starts the Norad Relay container while preserving configuration data.",
			Action:      restartHandler,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "dir",
					Value: "/etc/norad.d",
					Usage: "`DIRECTORY` with Norad configuration",
				},
				cli.BoolFlag{
					Name:  "force, f",
					Usage: "Do not prompt for confirmation before restarting the Relay.",
				},
			},
		},
		{
			Name:        "update",
			Usage:       "updates the Norad Relay to the latest version",
			Description: "Updates the Norad Relay image to the latest version while preserving configuration data.",
			Action:      updateHandler,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "dir",
					Value: "/etc/norad.d",
					Usage: "`DIRECTORY` with Norad configuration",
				},
				cli.BoolFlag{
					Name:  "force, f",
					Usage: "Do not prompt for confirmation before updating the Relay.",
				},
			},
		},
		{
			Name:        "reset",
			Usage:       "reinstalls the Norad Relay",
			Description: "Reinstalls and resets the configuration of the Norad Relay.",
			Action:      resetHandler,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "dir",
					Value: "/etc/norad.d",
					Usage: "installation `DIRECTORY` for Relay configuration",
				},
				cli.StringFlag{
					Name:  "relay-image",
					Value: "norad-registry.cisco.com:5000/relay:latest",
					Usage: "The `IMAGE` name of the Norad Relay Docker image",
				},
				cli.StringFlag{
					Name:   "organization-token",
					Usage:  "[REQUIRED] The `TOKEN` of the Organization for the Relay",
					EnvVar: "NORAD_ORGANIZATION_TOKEN",
				},
				cli.StringFlag{
					Name:  "api-uri",
					Value: "https://norad.cisco.com:8443/v1",
					Usage: "The `URI` and path of the associated Norad instance",
				},
				cli.StringFlag{
					Name:  "queue-user",
					Value: "norad-consumer",
					Usage: "The RabbitMQ `USER` for authenticating with the scan queue",
				},
				cli.StringFlag{
					Name:  "queue-password",
					Value: "norad-consumer",
					Usage: "The RabbitMQ `PASSWORD` for authenticating with the scan queue",
				},
				cli.BoolFlag{
					Name:  "force, f",
					Usage: "Do not prompt for confirmation before reseting the Relay.",
				},
			},
		},
		{
			Name:        "system-check",
			Usage:       "perform a system diagnostics check for compatibility with the Norad Relay",
			Description: "Checks the ability to create a Docker client and reach necessary Norad cloud hosts.",
			Action:      systemCheckHandler,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "api-uri",
					Value: "norad.cisco.com:8443",
					Usage: "The `URI` and path of the associated Norad instance",
				},
				cli.StringSliceFlag{
					Name: "registry-uris",
					Value: &cli.StringSlice{
						"norad-registry.cisco.com:5000",
						"norad-registry.cisco.com:5001",
					},
					Usage: "Comma separated list of test registry `URIs`.",
				},
			},
		},
		{
			Name:        "fixup-config",
			Usage:       "attempts to generate a valid Relay Docker configuration file",
			Description: "Some installations of the Relay have invalid Docker configuraton files. This command attempts to correct them.",
			Action:      fixupConfigHandler,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "dir",
					Value: "/etc/norad.d",
					Usage: "`DIRECTORY` with Norad configuration",
				},
				cli.BoolFlag{
					Name:  "force, f",
					Usage: "Do not prompt for confirmation before attempting to fix configuration",
				},
			},
		},
		{
			Name:        "logs",
			Usage:       "prints the Relay logs",
			Description: "Convenience method to print the logs of the Relay Docker container.",
			Action:      logsHandler,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "container-name",
					Value: "norad_relay",
					Usage: "The `NAME` of the Norad Relay Docker container.",
				},
				cli.BoolFlag{
					Name:  "f, follow",
					Usage: "Wait for new logs when EOF is reached.",
				},
			},
		},
		{
			Name:        "fingerprint",
			Usage:       "prints the Relay key fingerprint",
			Description: "Convenience method to print the key fingerprint for verification in the Norad UI",
			Action:      fingerprintHandler,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "dir",
					Value: "/etc/norad.d",
					Usage: "`DIRECTORY` with Norad configuration",
				},
			},
		},
		{
			Name:        "tests",
			Usage:       "prints the container IDs for tests running on the Relay",
			Description: "Convenience method to print the container IDs of the tests running on the Relay.",
			Action:      testsHandler,
			Flags: []cli.Flag{
				cli.BoolFlag{
					Name:  "a, all",
					Usage: "Show all Norad tests, even exited ones.",
				},
			},
		},
		{
			Name:        "debug",
			Usage:       "enables and disables debug mode for the Relay",
			Description: "Enables and disables the Norad Relay debug mode.",
			Action:      debugHandler,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "dir",
					Value: "/etc/norad.d",
					Usage: "`DIRECTORY` with Norad configuration",
				},
				cli.BoolFlag{
					Name:  "disable",
					Usage: "Disable debug mode",
				},
				cli.BoolFlag{
					Name:  "no-cleanup",
					Usage: "Don't remove exited test containers when going to production mode",
				},
				cli.BoolFlag{
					Name:  "force, f",
					Usage: "Do not prompt for confirmation before changing the Relay configuration.",
				},
			},
		},
		{
			Name:        "status",
			Usage:       "prints the Relay status",
			Description: "Prints the current state of the Relay, including version.",
			Action:      statusHandler,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "dir",
					Value: "/etc/norad.d",
					Usage: "`DIRECTORY` with Norad configuration",
				},
				cli.StringFlag{
					Name:  "registry-uri",
					Value: "https://norad-registry.cisco.com:5000",
					Usage: "The `URI` of the associated Docker Registry where the Relay is stored.",
				},
				cli.StringFlag{
					Name:  "relay-image",
					Value: "relay:latest",
					Usage: "The `IMAGENAME` of the Relay Docker image.",
				},
			},
		},
	}
	return app
}
