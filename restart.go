package main

import (
	"fmt"
	"log"

	"gitlab.com/norad/relay-cli/relay"
	"gopkg.in/urfave/cli.v1"
)

func restartHandler(c *cli.Context) error {
	docker_client, err := relay.NewRelayDockerClient()
	if err != nil {
		panic(err)
	}
	enabler := relay.RelayEnabler{
		Config: relay.EnableConfig{
			ConfigDir: c.String("dir"),
			Force:     false,
		},
		Docker: relay.RelayDocker{
			Client: docker_client,
		},
	}

	disabler := relay.RelayDisabler{
		Docker: relay.RelayDocker{
			Client: docker_client,
		},
	}

	if c.Bool("force") || confirmRestart() {
		return relay.Restart(enabler, disabler)
	} else {
		log.Printf("Exiting without restarting.")
		return nil
	}
}

func confirmRestart() bool {
	fmt.Println("Are you sure you want to restart the Relay? Please enter 'yes' or 'no'.")
	return confirmAction()
}
